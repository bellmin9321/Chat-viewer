## Structure

App - Header - Friends/Chats page(Link)
| - Friends - Friends_list/search - detail

store - slice toolkit(reducer/action/type)

## App description

1. App을 크게 Header, Body(Friends)로 구성
2. Friends / Chats / Chat_page 3개의 page
3. configureStore toolkit을 이용해서 reducer 함수를 store에 저장
4. 3가지 reducer 함수(slice toolkit)

- dateSlice
- profileSlice
- talkSlice

5. store: configureStore toolkit

6. Friends_list component에서 Fetch(async/await)를 이용해 profile.json Mock data 받음
   (profile.json은 public 폴더에 있음)
7. 구현 기능: 검색, 정렬, 채팅 등
8. 기타: day.js, EllipsisText 라이브러리 사용

## 완성한 페이지 및 기능

![Friends](/readme-assets/friends_page.JPG)
![Chat_page](/readme-assets/chat_page.JPG)
![Chat_list](/readme-assets/chats_list.JPG)
![Search](/readme-assets/search.JPG)
![Sort](/readme-assets/sort.JPG)

## TODO

- [x] 친구 목록 페이지, 채팅 목록 페이지, 채팅 페이지가 있습니다.

- [x] 친구 목록 페이지에는 채팅 목록으로 이동할 수 있는 버튼이 있어야 합니다.

- [x] 친구 목록 페이지에는 친구 이름, 사진과 함께 채팅을 시작할 수 있는 버튼이 있어야 합니다.

- [x] 친구 목록 페이지에 있는 “대화하기” 버튼을 이용해 해당 친구와의 채팅 페이지로 바로 이동 할 수 있습니다.
      바닐라코딩 미드텀 I - "왜 대답이 없어?"

- [x] 친구 목록은 이름을 기준으로 오름차순 또는 내림차순 정렬할 수 있는 기능이 있어야 합니 다.

- [x] 친구 목록에 있는 검색창을 이용해 친구 이름을 기준으로 검색할 수 있는 기능이 있어야 합 니다.

- [ ] 채팅 목록에는 진행 중인 채팅이 날짜 순으로 나열되어야 합니다. (최신날짜가 상위)

- [x] 채팅 목록 페이지에는 친구 목록으로 이동할 수 있는 버튼이 있어야 합니다.

- [x] 채팅 목록에는 진행 중인 채팅의 친구 이름, 가장 최신 메시지의 첫 30글자, 가장 최신 메시 지 전송 날짜가 표기되어야 합니다.

- [x] 채팅 목록에서 채팅을 선택하면 채팅 페이지로 이동합니다.

- [ ] 채팅 페이지에는 상대방과 나눈 대화가 시간 순으로 나열되어야 합니다. 상대방 이름, 메시 지 내용, 전송 날짜 및 시간이 모두 표기되어야 합니다.

- [ ] 채팅 페이지는 별도 URL이 없습니다.

- [ ] 채팅 페이지에서 메시지를 전송할 경우, 현재 채팅 페이지와 채팅 목록 페이지에 새로운 메 시지가 반영되어야 합니다. (서버가 없으므로 전송된 메시지를 화면에 표기하는 것은 가능하지 만, 메시지 수신은 불가능합니다.)

- [x] 페이지 새로고침에 대한 대응은 하지 않아도 괜찮습니다.

- [x] 초기 어플리케이션 상태에서 나타나는 친구 및 채팅 데이터는 직접 JSON 파일 형식으로 만들어 사용하세요.
