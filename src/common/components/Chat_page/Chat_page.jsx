import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Content_contatiner } from "../../../public/Content_container/Content_contatiner";
import styled from "styled-components";
import { addTalk, addTalkData } from "../../../features/talksSlice";

export default function Chat_page() {
  const [ talk, setTalk ] = useState("");
  const [ isTalkList, setIsTalkList ] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { profileId } = useParams();
  const { profiles } = useSelector((state) => state.profiles);
  const { date } = useSelector((state) => state.date);
  const { byId } = useSelector((state) => state.talks);
  const dateArr = date.split(" ");
  const dataList = Object.values(byId);
  
  const target = profiles.filter((profile) => {
    if (Number(profileId) === profile.id) {
      return profile;
    }
  });

  const { name, say, img } = target[0];
  const now = new Date();

  const dateObj = {
    id: `${now.getFullYear()}-${(now.getMonth() + 1)}-${now.getDate()}-${now.getHours()}-${now.getMinutes()}-${now.getSeconds()}`,
    profileId: profileId,
    talk: talk,
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsTalkList(true);
    dispatch(addTalk(talk));
    dispatch(addTalkData(dateObj));
    setTalk("");
  };

  return (
    <Content_contatiner>
      <ChatPageWrapper>
        <ChatNav>
          <button
            onClick={() => {
              navigate(-1);
            }}
          >back</button>  
          <span className="nav_name">{name}님과의 대화</span>
          <span className="blank" />
        </ChatNav>
        <Chat_body>
          <Chat_body_inner>
            <div className="other_box">
              <img className="chat_img" src={img} />
              <div className="chat_name">{name}</div>
              <span className="chat_desc">{say}</span>
              <span className="date">
                {`${dateArr[0]}년 ${dateArr[1]}월 ${dateArr[2]}일 ${dateArr[3]}시 ${dateArr[4]}분 ${dateArr[5]}초`}
              </span>
            </div>
            <div className="mytalk_box">
              {dataList.map((data) => {
                const date = data.id.split("-")

                return (
                  <div>
                    <div key={data.id} className="myTalk">
                      {data.talk}
                    </div>
                    <div className="date">
                      {isTalkList && `
                        ${date[0]}년 ${date[1]}월
                        ${date[2]}일 ${date[3]}시
                        ${date[4]}분 ${date[5]}초
                      `}
                    </div>
                  </div>
                )          
              })}
            </div>
          </Chat_body_inner>
        </Chat_body>
        <form 
          onSubmit={handleSubmit}
          className="input_box"
        >
          <input 
            type="text"
            value={talk}
            onChange={(e) => setTalk(e.target.value)}
            placeholder="채팅 메시지를 입력하세요"
          />
        </form>
      </ChatPageWrapper>
      <ChatPageBottom />
    </Content_contatiner>
  );
}

const ChatPageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 90%;

  .input_box {
    width: 100%;
    height: 10%;

    input {
      border: none;
      width: 95%;
      height: 97%;
      font-size: 20px;
    }
  }
`;

const ChatPageBottom = styled.div`
  height: 10%;
  width: 100%;
  background-color: black;
`;

const ChatNav = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 10%;
  margin-top: 2px;
  background-color: white;

  button {
    border: none;
    font-size: 20px;
    width: 20%;
    height: 100%;
    cursor: pointer;

    :hover {
      color: white;
      background-color: gray;
    }
  }

  .nav_name {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 20px;
    font-weight: bold;
    height: 100%;
    width: 60%;
  }

  .blank {
    width: 20%;
  }
`;

const Chat_body = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 80%;
  overflow: hidden;
  background-color: lightblue;
  overflow-y: auto;
`;

const Chat_body_inner = styled.div`
  width: 90%;
  height: 95%;
  align-items: center;
  justify-content: space-between;

  .chat_img {
    width: 50px;
    height: 50px;
    border-radius: 50%;
  }

  .other_box {
    width: 60%;
    
    .chat_name {
      font-size: 20px;
      padding-bottom: 5px;
    }
    
    .chat_desc {
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 18px;
      height: 10%;
      margin-bottom: 4px;
      border-radius: 5px;
      background-color: yellow;
    }

    .date {
      float: left;
      font-size: 12px;
      margin-bottom: 10px;
    } 
  }


  .mytalk_box {
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 60%;
    float: right;
    
    .myTalk {
      float: right;
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 18px;
      width: 100%;
      margin-bottom: 3px;
      border-radius: 5px;
      background-color: yellow;
    }

    .date {
      float: right;
      font-size: 12px;
      padding-bottom: 10px;
    }
  }
`;