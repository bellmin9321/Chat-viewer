import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { Content_contatiner } from "../../../public/Content_container/Content_contatiner";
import { Friend, FriendDetailWrapper, Friend_info } from "../Friend_detail/Friend_detail";
import { FriendsListWrapper } from "../Friends_list/Friends_list";
import { useNavigate } from "react-router-dom"; 
import EllipsisText from 'react-ellipsis-text/lib/components/EllipsisText';

export default function Chats() {
  const navigate = useNavigate();
  const { date } = useSelector((state) => state.date);
  const { talkList } = useSelector((state) => state.talks);
  const dateArr = date.split(" ");

  return (
    <Content_contatiner>
      <FriendsListWrapper>
          {talkList.map((chat) => (
            <FriendDetailWrapper key={chat.id}>
              <hr />
              <Friend onClick={() => navigate(`/chats/${chat.id}`)}>
                <Friend_info>
                  <img src={chat.img} />
                  <div>
                    <div>{chat.name}</div>
                    <div>
                      <EllipsisText 
                        text={chat.desc} 
                        length={30}
                      />
                    </div>
                  </div>
                </Friend_info>
                <Date_info>
                  {`${dateArr[0]}년 ${dateArr[1]}월 ${dateArr[2]}일`}
                </Date_info>
              </Friend>
            </FriendDetailWrapper>
          ))}
      </FriendsListWrapper>
    </Content_contatiner>
  );
}

const Date_info = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 100%;
  width: 30%;
  font-size: 12px;
`;
