import React, { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import styled from "styled-components";
import { searchProfiles, sortProfiles } from '../../../features/profilesSlice';

export function Friends_search() {
  const dispatch = useDispatch();
  const { searchKeyword } = useSelector((state) => state.profiles);
  const { isSorted } = useSelector((state) => state.profiles);

  const handleSearch = (e) => {
    e.preventDefault();
    dispatch(searchProfiles(""));
  }

  const toggleSort = () => {
    dispatch(sortProfiles(isSorted));
  }

  return (
    <Content_header>
      <form onSubmit={handleSearch}>
        <input
          type="text"
          value={searchKeyword}
          onChange={
              (e) => dispatch(searchProfiles(e.target.value)
            )}
          placeholder="친구 검색"
        />
        <Alignment_btn
          onClick={toggleSort}
        >이름순 정렬</Alignment_btn>
      </form>
    </Content_header>
  )
};

  
export const Content_header = styled.nav`
  width: 90%;
  height: 10%;
  align-items: center;
  
  form {
    height: 100%;
    margin-top: 5px;
    display: flex;
    flex-direction:row;
    align-items: center;
    justify-content: space-between;

    input {
      width: 50%;
      height: 50%;
    }
  }
`;

const Alignment_btn = styled.button`
background-color: white;
border: 1px solid lightgray;
cursor: pointer;

  :hover {
    color: white;
    border: 1px solid gray;
    background-color: gray;
  }
`;