import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import Friend_detail from "../Friend_detail/Friend_detail";
import { addProfiles } from "../../../features/profilesSlice";

export function Friends_list() {
  const { profiles } = useSelector((state) => state.profiles);
  const { searchKeyword } = useSelector((state) => state.profiles);
  const { isSorted } = useSelector((state) => state.profiles);
  const dispatch = useDispatch();

  const requestProfiles = async () => {
    const res = await fetch("/data/profiles.json");
    const data = await res.json();

    dispatch(addProfiles(data));

    return data;
  };

  const sortedProfiles = [].concat(profiles)
      .sort((a, b) => a.name > b.name ? 1 : -1);
      
  const toggleProfiles = () => {
    return isSorted ? sortedProfiles : profiles;
  };

  useEffect(() => {
    requestProfiles();
  }, [isSorted]);

  return (
    <FriendsListWrapper>
      {profiles && toggleProfiles().map((profile) => {
        if (searchKeyword === "") {
          return (
            <Friend_detail
              key={profile.id}
              id={profile.id}
              name={profile.name}
              img={profile.img}
              desc={profile.desc}
            />
          );
        } else if (
          profile.name.toLowerCase().includes(searchKeyword.toLowerCase())) {
            return (
              <Friend_detail
                key={profile.id}
                id={profile.id}
                name={profile.name}
                img={profile.img}
                desc={profile.desc}
              />
            );
        }
      })}
    </FriendsListWrapper>
  );
}
  
export const FriendsListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 85%;
  width: 90%;
  margin-bottom: 30px;
`;
