import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <HeaderWrapper>
      <div className="upper_container"></div>
      <Box_container>
        <Link to="/friends">
          <Friends_Box>Friends</Friends_Box>
        </Link>
        <Link to="/chats">
          <Chats_Box>Chats</Chats_Box>
        </Link>
      </Box_container>
    </HeaderWrapper>
  );
}

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 20%;
  width: 100%;
  border-radius: 50px 50px 0 0;
  background-color: black;

  .upper_container {
    height: 50%;
    width: 100%;
  }
`;

const Box_container = styled.div`
  display: flex;
  width: 100%;
  height: 50%;

  a {
    text-decoration: none;
    color: black;
    width: 100%;
    height: 100%;
  }
`;

const Friends_Box = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: lightgray;
  font-size: 25px;
  height: 100%;
  width: 100%;
  cursor: pointer;
  border: 1px solid black;


  :hover {
    color: white;
    background-color: gray;
  }
`;
  
const Chats_Box = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: lightgray;
  font-size: 25px;
  height: 100%;
  width: 100%;
  cursor: pointer;
  border: 1px solid black;

  :hover {
    color: white;
    background-color: gray;
  }
`;
