import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { addDate } from "../../../features/dateSlice";
import { addTalk, addTargetId } from '../../../features/talksSlice';

export default function Friend_detail({ id, name, img }) {
  const dispatch = useDispatch();
  const { profiles } = useSelector((state) => state.profiles);

  const handleClick = () => {
    profiles.filter((profile) => {
      if (profile.id === id) {
        dispatch(addTalk(profile));
        dispatch(addTargetId(id));
      }
        dispatch(addDate({}));
    });
  };

  return (
    <FriendDetailWrapper>
      <hr />
      <Friend>
        <Friend_info>
          <img src={img} />
          <div>{name}</div>
        </Friend_info>
        <Friend_talk>
          <Link 
            to={`/chats/${id}`}
          >
            <button onClick={handleClick}
            >채팅하기</button>
          </Link>
        </Friend_talk>
      </Friend>
    </FriendDetailWrapper>
  );
}

export const FriendDetailWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 20%;
  
  hr {
  border: none;
  width: 100%;
  height: 1px;
  background-color: lightgray;
  }
`;

export const Friend = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  height: 60px;
  width: 100%;

  :hover {
    cursor: pointer;
    background-color: lightgray;
  }
`;

export const Friend_info = styled.div`
  font-size: 13px;
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 100%;
  width: 70%;

  img {
  border-radius: 50%;
  margin-right: 10px;
  width: 20%;
  height: 80%;
  }
`;

const Friend_talk = styled.div`
  width: 30%;

  button {
    border: 1px solid lightgray;
    background-color: white;
    height: 100%;
    width: 100%;
    cursor: pointer;

    :hover {
      color: white;
      border: 1px solid gray;
      background-color: gray;
    }
  }
`;
