import React from "react";
import styled from "styled-components";

export const Content_contatiner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  border-radius: 0 0 50px 50px;
  overflow-y: auto;
`;
