import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  profiles: null,
  searchKeyword: "",
  isSorted: false,
};

const profilesSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {
    addProfiles(state, { payload }) {
      state.profiles = payload;
    },
    searchProfiles(state, { payload }) {
      state.searchKeyword = payload;
    },
    sortProfiles(state, { payload }) {
      state.isSorted = !payload;
    }
  }
});

export const {
  addProfiles, 
  searchProfiles,
  sortProfiles 
} = profilesSlice.actions;

export default profilesSlice.reducer;