import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  targetId: 0,
  talkList: [],
  byId: {},
  allIds: [],
};

const talksSlice = createSlice({
  name: "talk",
  initialState,
  reducers: {
    addTalk({ talkList }, { payload }) {
      talkList.push(payload);
    },
    addTalkData({ byId, allIds }, { payload }) {
      byId[payload.id] = payload;
      allIds.push(payload.id);
    },
    addTargetId({ targetId }, { payload }) {
      targetId = payload;
    }
  },
});

export const { addTalk, addTalkData, addTargetId } = talksSlice.actions;

export default talksSlice.reducer;

