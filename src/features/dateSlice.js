import { createSlice } from "@reduxjs/toolkit";
import dayjs from "dayjs";

const today = dayjs();

const initialState = {
  date: today.format("YYYY MM DD HH mm ss"),
  byId: {},
  allIds: [],
};

const dateSlice = createSlice({
  name: "dateAdd",
  initialState  ,
  reducers: {
    addDate(state, { payload }) {
      state.byId[payload.id] = payload;
      state.allIds.push(payload.id);
    },
  },
});

export const { addDate } = dateSlice.actions;

export default dateSlice.reducer;
