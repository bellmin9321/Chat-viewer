import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import logger from 'redux-logger';
import dateSlice from '../features/dateSlice';
import profilesSlice from '../features/profilesSlice';
import talksSlice from '../features/talksSlice';

const store = configureStore({
  reducer: {
    date: dateSlice,
    profiles: profilesSlice,
    talks: talksSlice,
  }, 
  middleWare: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});

export default store;
