import Header from "../common/components/Header/Header";
import { Routes, Route } from "react-router-dom";
import styled from "styled-components";
import Friends from "../common/components/Friends/Friends";
import Chats from "../common/components/Chats/Chats";
import { useEffect, useState } from 'react';
import Chat_page from '../common/components/Chat_page/Chat_page';
import "./App.css"

export default function App() {  
  return (
    <AppContainer>
      <hr />
      <InnerContainer>
        <Header />
        <Routes>
          <Route 
            path="/friends" 
            element={<Friends />} 
          />
          <Route 
            path="/" 
            element={<Friends />} 
          />
          <Route 
            path="/chats" 
            element={<Chats />} />
          <Route 
            path="/chats/:profileId" 
            element={<Chat_page />}
          />
        </Routes>
      </InnerContainer>
    </AppContainer>
  );
}

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 800px;
`;

const InnerContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: 3px solid black;
  border-radius: 55px;
  width: 20%;
  height: 80%;
`;
